import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    width: '100%',
    marginTop: '10px',
    textAlign: 'center',
  },
});

export default function Result(props) {
  const classes = useStyles();

  return (
    <Typography
      className={classes.root}
      variant="h3"
      component="h3"
      gutterBottom
    >
      {props.result} {props.unit}
    </Typography>
  );
}
