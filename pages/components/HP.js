import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Result from './Result';
import { lookupHP } from '../../scripts/helpers';
import {
  threePhase115currs,
  threePhase200currs,
  threePhase208currs,
  threePhase230currs,
  threePhase460currs,
  threePhase575currs,
  singlePhase115currs,
  singlePhase230currs,
} from '../../scripts/flcData';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
    flexGrow: 1,
  },
  formControl: {
    margin: theme.spacing(2),
    minWidth: '100px',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  containerItem: {
    textAlign: 'center',
  },
}));

export default function HP() {
  const classes = useStyles();
  const [voltage, setVoltage] = React.useState('');
  const [phase, setPhase] = React.useState('');
  const [hp, setHP] = React.useState('');
  const [curr, setCurr] = React.useState('');
  let currMenu;

  const handleVoltage = (event) => {
    setVoltage(event.target.value);
  };

  const handlePhase = (event) => {
    setPhase(event.target.value);
  };

  const handleCurr = (event) => {
    setCurr(event.target.value);
  };

  const handleClick = (event) => {
    const newHP = lookupHP(phase, curr, voltage);
    setHP(newHP);
  };

  switch (phase) {
    case 1:
      switch (voltage) {
        case 115:
          currMenu = singlePhase115currs.map((val, i) => (
            <MenuItem key={i} value={val}>
              {val}
            </MenuItem>
          ));
          break;
        case 230:
          currMenu = singlePhase230currs.map((val, i) => (
            <MenuItem key={i} value={val}>
              {val}
            </MenuItem>
          ));
          break;
        default:
          break;
      }
      break;
    case 2:
      switch (voltage) {
        case 115:
          currMenu = threePhase115currs.map((val, i) => (
            <MenuItem key={i} value={val}>
              {val}
            </MenuItem>
          ));
          break;
        case 200:
          currMenu = threePhase200currs.map((val, i) => (
            <MenuItem key={i} value={val}>
              {val}
            </MenuItem>
          ));
          break;
        case 208:
          currMenu = threePhase208currs.map((val, i) => (
            <MenuItem key={i} value={val}>
              {val}
            </MenuItem>
          ));
          break;
        case 230:
          currMenu = threePhase230currs.map((val, i) => (
            <MenuItem key={i} value={val}>
              {val}
            </MenuItem>
          ));
          break;
        case 460:
          currMenu = threePhase460currs.map((val, i) => (
            <MenuItem key={i} value={val}>
              {val}
            </MenuItem>
          ));
          break;
        case 575:
          currMenu = threePhase575currs.map((val, i) => (
            <MenuItem key={i} value={val}>
              {val}
            </MenuItem>
          ));
          break;

        default:
          break;
      }
    default:
      break;
  }

  return (
    <Grid
      container
      className={classes.root}
      spacing={2}
      direction="row"
      justify="space-evenly"
      alignItems="center"
    >
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <FormControl className={classes.formControl}>
          <InputLabel shrink id="demo-simple-select-placeholder-label-label">
            Motor Phase
          </InputLabel>
          <Select
            labelId="demo-simple-select-placeholder-label-label"
            id="demo-simple-select-placeholder-label"
            value={phase}
            onChange={handlePhase}
            displayEmpty
            className={classes.selectEmpty}
          >
            <MenuItem value="" disabled>
              Phase
            </MenuItem>
            <MenuItem value={1}>Single-Phase</MenuItem>
            <MenuItem value={2}>Three-Phase</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <FormControl className={classes.formControl} disabled={phase === ''}>
          <InputLabel shrink id="demo-simple-select-placeholder-label-label">
            Voltage
          </InputLabel>
          <Select
            labelId="demo-simple-select-placeholder-label-label"
            id="demo-simple-select-placeholder-label"
            value={voltage}
            onChange={handleVoltage}
            displayEmpty
            className={classes.selectEmpty}
          >
            <MenuItem value="" disabled>
              Voltage
            </MenuItem>
            <MenuItem value={115}>115</MenuItem>
            {phase === 2 && <MenuItem value={200}>200</MenuItem>}
            {phase === 2 && <MenuItem value={208}>208</MenuItem>}
            <MenuItem value={230}>230</MenuItem>
            {phase === 2 && <MenuItem value={460}>460</MenuItem>}
            {phase === 2 && <MenuItem value={575}>575</MenuItem>}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <FormControl className={classes.formControl} disabled={phase === ''}>
          <InputLabel shrink id="demo-simple-select-placeholder-label-label">
            FLC
          </InputLabel>
          <Select
            labelId="demo-simple-select-placeholder-label-label"
            id="demo-simple-select-placeholder-label"
            value={curr}
            onChange={handleCurr}
            displayEmpty
            className={classes.selectEmpty}
          >
            <MenuItem value="" disabled>
              Full Load Current (A)
            </MenuItem>
            {currMenu}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <Button variant="contained" onClick={handleClick}>
          CALCULATE
        </Button>
      </Grid>
      <Result result={hp} unit={'HP'} />
    </Grid>
  );
}
