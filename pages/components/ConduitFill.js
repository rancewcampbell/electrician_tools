import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Result from './Result';
import { lookupConduitFill } from '../../scripts/helpers';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
    flexGrow: 1,
  },
  formControl: {
    margin: theme.spacing(2),
    minWidth: '100px',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  containerItem: {
    textAlign: 'center',
  },
}));

export default function ConduitFill() {
  const classes = useStyles();
  const [conductor, setConductor] = React.useState('');
  const [conduit, setConduit] = React.useState('');
  const [fill, setFill] = React.useState('');

  const handleConductor = (event) => {
    setConductor(event.target.value);
  };

  const handleConduit = (event) => {
    setConduit(event.target.value);
  };

  const handleClick = (event) => {
    const newFill = lookupConduitFill(conductor, conduit);
    setFill(newFill);
  };

  return (
    <Grid
      container
      className={classes.root}
      spacing={2}
      direction="row"
      justify="space-evenly"
      alignItems="center"
    >
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <FormControl className={classes.formControl}>
          <InputLabel shrink id="demo-simple-select-placeholder-label-label">
            Conductor
          </InputLabel>
          <Select
            labelId="demo-simple-select-placeholder-label-label"
            id="demo-simple-select-placeholder-label"
            value={conductor}
            onChange={handleConductor}
            displayEmpty
            className={classes.selectEmpty}
          >
            <MenuItem value="" disabled>
              Conductor
            </MenuItem>
            <MenuItem value={'14'}>14AWG</MenuItem>
            <MenuItem value={'12'}>12AWG</MenuItem>
            <MenuItem value={'10'}>10AWG</MenuItem>
            <MenuItem value={'8'}>8AWG</MenuItem>
            <MenuItem value={'6'}>6AWG</MenuItem>
            <MenuItem value={'4'}>4AWG</MenuItem>
            <MenuItem value={'3'}>3AWG</MenuItem>
            <MenuItem value={'2'}>2AWG</MenuItem>
            <MenuItem value={'1'}>1AWG</MenuItem>
            <MenuItem value={'1/0'}>1/0AWG</MenuItem>
            <MenuItem value={'2/0'}>2/0AWG</MenuItem>
            <MenuItem value={'3/0'}>3/0AWG</MenuItem>
            <MenuItem value={'4/0'}>4/0AWG</MenuItem>
            <MenuItem value={'250'}>250kcmil</MenuItem>
            <MenuItem value={'300'}>300kcmil</MenuItem>
            <MenuItem value={'350'}>350kcmil</MenuItem>
            <MenuItem value={'400'}>400kcmil</MenuItem>
            <MenuItem value={'450'}>450kcmil</MenuItem>
            <MenuItem value={'500'}>500kcmil</MenuItem>
            <MenuItem value={'600'}>600kcmil</MenuItem>
            <MenuItem value={'700'}>700kcmil</MenuItem>
            <MenuItem value={'750'}>750kcmil</MenuItem>
            <MenuItem value={'800'}>800kcmil</MenuItem>
            <MenuItem value={'900'}>900kcmil</MenuItem>
            <MenuItem value={'1000'}>1000kcmil</MenuItem>
            <MenuItem value={'1250'}>1250kcmil</MenuItem>
            <MenuItem value={'1500'}>1500kcmil</MenuItem>
            <MenuItem value={'1750'}>1750kcmil</MenuItem>
            <MenuItem value={'2000'}>2000kcmil</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <FormControl className={classes.formControl}>
          <InputLabel shrink id="demo-simple-select-placeholder-label-label">
            Conduit Size
          </InputLabel>
          <Select
            labelId="demo-simple-select-placeholder-label-label"
            id="demo-simple-select-placeholder-label"
            value={conduit}
            onChange={handleConduit}
            displayEmpty
            className={classes.selectEmpty}
          >
            <MenuItem value="" disabled>
              Conduit
            </MenuItem>
            <MenuItem value={0.5}>1/2" (16)</MenuItem>
            <MenuItem value={0.75}>3/4" (21)</MenuItem>
            <MenuItem value={1}>1" (27)</MenuItem>
            <MenuItem value={1.25}>1 1/4" (35)</MenuItem>
            <MenuItem value={1.5}>1 1/2" (41)</MenuItem>
            <MenuItem value={2}>2" (53)</MenuItem>
            <MenuItem value={2.5}>2 1/2" (63)</MenuItem>
            <MenuItem value={3}>3" (78)</MenuItem>
            <MenuItem value={3.5}>3 1/2" (91)</MenuItem>
            <MenuItem value={4}>4" (103)</MenuItem>
            <MenuItem value={4.5}>4 1/2" (116)</MenuItem>
            <MenuItem value={5}>5" (129)</MenuItem>
            <MenuItem value={6}>6" (155)</MenuItem>
            <MenuItem value={8}>8" (200)</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <Button variant="contained" onClick={handleClick}>
          CALCULATE
        </Button>
      </Grid>
      <Result result={fill} unit={'Conductors'} />
    </Grid>
  );
}
