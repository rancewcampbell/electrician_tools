import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    width: '100%',
    marginTop: '10px',
    textAlign: 'center',
  },
});

export default function Disclaimer(props) {
  const classes = useStyles();

  return (
    <Typography className={classes.root} component="body2" gutterBottom>
      *all calculations based on Canadian Electrical Code Part I 2018
    </Typography>
  );
}
