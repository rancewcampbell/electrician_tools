import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Result from './Result';
import { sizeOvercurrent } from '../../scripts/helpers';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
    flexGrow: 1,
  },
  formControl: {
    margin: theme.spacing(2),
    minWidth: '100px',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  containerItem: {
    textAlign: 'center',
  },
  margin: {
    margin: theme.spacing(1),
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    minWidth: '100px',
  },
}));

export default function Overloads() {
  const classes = useStyles();
  const [flc, setFlc] = React.useState('');
  const [flcError, setFlcError] = React.useState(false);
  const [ocType, setOcType] = React.useState('ntd');
  const [oc, setOc] = React.useState('');

  const handleChange = (event) => {
    const flc = event.target.value;
    if (flc <= 0) {
      setFlcError(true);
    } else {
      setFlcError(false);
    }
    setFlc(flc);
  };

  const handleOcType = (event) => {
    setOcType(event.target.value);
  };

  const handleClick = (event) => {
    if (flcError || !flc) return;
    const newOc = sizeOvercurrent(flc, ocType);
    setOc(newOc);
  };

  return (
    <Grid
      container
      className={classes.root}
      spacing={2}
      direction="row"
      justify="space-evenly"
      alignItems="center"
    >
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <FormControl
          className={(classes.margin, classes.withoutLabel, classes.textField)}
        >
          <TextField
            error={flcError}
            id={flcError ? 'outlined-error-helper-text' : 'standard-number'}
            label="Full Load Current"
            helperText={flcError ? 'Incorrect entry.' : 'Amps'}
            type="number"
            value={flc}
            onChange={handleChange}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <FormControl className={classes.formControl}>
          <InputLabel shrink id="demo-simple-select-placeholder-label-label">
            Overcurrent Type
          </InputLabel>
          <Select
            labelId="demo-simple-select-placeholder-label-label"
            id="demo-simple-select-placeholder-label"
            value={ocType}
            onChange={handleOcType}
            displayEmpty
            className={classes.selectEmpty}
          >
            <MenuItem value="" disabled>
              Overcurrent Type
            </MenuItem>
            <MenuItem value={'td'}>Time Delay Fuse</MenuItem>
            <MenuItem value={'ntd'}>Non-Time Delay Fuse</MenuItem>
            <MenuItem value={'cb'}>Circuit Breaker</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <Button variant="contained" onClick={handleClick}>
          CALCULATE
        </Button>
      </Grid>
      <Result result={oc} unit={'Amps'} />
    </Grid>
  );
}
