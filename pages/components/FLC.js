import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Result from './Result';
import { lookupFLC } from '../../scripts/helpers';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
    flexGrow: 1,
  },
  formControl: {
    margin: theme.spacing(2),
    minWidth: '100px',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  containerItem: {
    textAlign: 'center',
  },
}));

export default function FLC() {
  const classes = useStyles();
  const [voltage, setVoltage] = React.useState('');
  const [phase, setPhase] = React.useState('');
  const [hp, setHP] = React.useState('');
  const [curr, setCurr] = React.useState('');

  const handleVoltage = (event) => {
    setVoltage(event.target.value);
  };

  const handlePhase = (event) => {
    setPhase(event.target.value);
  };

  const handleHP = (event) => {
    setHP(event.target.value);
  };

  const handleClick = (event) => {
    const newCurr = lookupFLC(phase, hp, voltage);
    setCurr(newCurr);
  };

  return (
    <Grid
      container
      className={classes.root}
      spacing={2}
      direction="row"
      justify="space-evenly"
      alignItems="center"
    >
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <FormControl className={classes.formControl}>
          <InputLabel shrink id="demo-simple-select-placeholder-label-label">
            Motor Phase
          </InputLabel>
          <Select
            labelId="demo-simple-select-placeholder-label-label"
            id="demo-simple-select-placeholder-label"
            value={phase}
            onChange={handlePhase}
            displayEmpty
            className={classes.selectEmpty}
          >
            <MenuItem value="" disabled>
              Phase
            </MenuItem>
            <MenuItem value={1}>Single-Phase</MenuItem>
            <MenuItem value={2}>Three-Phase</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <FormControl className={classes.formControl} disabled={phase === ''}>
          <InputLabel shrink id="demo-simple-select-placeholder-label-label">
            Voltage
          </InputLabel>
          <Select
            labelId="demo-simple-select-placeholder-label-label"
            id="demo-simple-select-placeholder-label"
            value={voltage}
            onChange={handleVoltage}
            displayEmpty
            className={classes.selectEmpty}
          >
            <MenuItem value="" disabled>
              Voltage
            </MenuItem>
            <MenuItem value={115}>115</MenuItem>
            {phase === 2 && <MenuItem value={200}>200</MenuItem>}
            {phase === 2 && <MenuItem value={208}>208</MenuItem>}
            <MenuItem value={230}>230</MenuItem>
            {phase === 2 && <MenuItem value={460}>460</MenuItem>}
            {phase === 2 && <MenuItem value={575}>575</MenuItem>}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <FormControl className={classes.formControl} disabled={phase === ''}>
          <InputLabel shrink id="demo-simple-select-placeholder-label-label">
            HP
          </InputLabel>
          <Select
            labelId="demo-simple-select-placeholder-label-label"
            id="demo-simple-select-placeholder-label"
            value={hp}
            onChange={handleHP}
            displayEmpty
            className={classes.selectEmpty}
          >
            <MenuItem value="" disabled>
              Horse Power
            </MenuItem>
            {phase === 1 && <MenuItem value={0.16}>1/6</MenuItem>}
            {phase === 1 && <MenuItem value={0.25}>1/4</MenuItem>}
            {phase === 1 && <MenuItem value={0.33}>1/3</MenuItem>}
            <MenuItem value={0.5}>1/2</MenuItem>
            <MenuItem value={0.75}>3/4</MenuItem>
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={1.5}>1-1/2</MenuItem>
            <MenuItem value={2}>2</MenuItem>
            <MenuItem value={3}>3</MenuItem>
            <MenuItem value={5}>5</MenuItem>
            <MenuItem value={7.5}>7-1/2</MenuItem>
            <MenuItem value={10}>10</MenuItem>
            {phase === 2 && <MenuItem value={15}>15</MenuItem>}
            {phase === 2 && <MenuItem value={20}>20</MenuItem>}
            {phase === 2 && <MenuItem value={25}>25</MenuItem>}
            {phase === 2 && <MenuItem value={30}>30</MenuItem>}
            {phase === 2 && <MenuItem value={40}>40</MenuItem>}
            {phase === 2 && <MenuItem value={50}>50</MenuItem>}
            {phase === 2 && <MenuItem value={60}>60</MenuItem>}
            {phase === 2 && <MenuItem value={75}>75</MenuItem>}
            {phase === 2 && <MenuItem value={100}>100</MenuItem>}
            {phase === 2 && <MenuItem value={125}>125</MenuItem>}
            {phase === 2 && <MenuItem value={150}>150</MenuItem>}
            {phase === 2 && <MenuItem value={200}>200</MenuItem>}
            {phase === 2 && <MenuItem value={250}>250</MenuItem>}
            {phase === 2 && <MenuItem value={300}>300</MenuItem>}
            {phase === 2 && <MenuItem value={350}>350</MenuItem>}
            {phase === 2 && <MenuItem value={400}>400</MenuItem>}
            {phase === 2 && <MenuItem value={450}>450</MenuItem>}
            {phase === 2 && <MenuItem value={500}>500</MenuItem>}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={3} className={classes.containerItem}>
        <Button variant="contained" onClick={handleClick}>
          CALCULATE
        </Button>
      </Grid>
      <Result result={curr} unit={'Amps'} />
    </Grid>
  );
}
