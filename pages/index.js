import React from 'react';
import Head from 'next/head';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Menu from './components/Menu';
import Disclaimer from './components/Disclaimer';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    flexGrow: 1,
    margin: 'auto',
  },
  containerItem: {
    textAlign: 'center',
  },
}));

export default function Home() {
  const classes = useStyles();
  const [titleSize, setTitleSize] = React.useState('h1');

  React.useEffect(() => {
    setTitleSize(window.innerWidth > 500 ? 'h1' : 'h2');
  }, []);

  return (
    <div>
      <Head>
        <title>Electrician Tools</title>
      </Head>

      <main>
        <Grid
          container
          className={classes.root}
          spacing={2}
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={12} className={classes.containerItem}>
            <Typography variant={titleSize} component={titleSize} gutterBottom>
              Electrician Tools
            </Typography>
            <Disclaimer />
          </Grid>
          <Grid item xs={12} className={classes.containerItem}>
            <Menu />
          </Grid>
        </Grid>
      </main>
    </div>
  );
}
