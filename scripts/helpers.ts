import { SSL_OP_NO_TLSv1_2 } from 'constants';
import { threePhaseFLC, singlePhaseFLC } from './flcData';
import { table6a } from './table6A';

const singlePhaseHp = [0.16, 0.25, 0.33, 0.5, 0.75, 1, 1.5, 2, 3, 5, 7.5, 10];
const threePhaseHp = [0.5, 0.75, 1, 1.5, 2, 3, 5, 7.5, 10, 15, 20, 25, 30, 40, 50, 60, 75, 100, 125, 150, 200, 250, 300, 350, 400, 450, 500];
const singlePhaseVolt = [115, 230];
const threePhaseVolt = [115, 200, 208, 230, 460, 575];


export const lookupFLC = (phase: 1 | 2, hp: number, voltage: number) => {
    let flc = 0;
    const hpStr = hp.toString();

    if (phase === 1 && singlePhaseHp.includes(hp) && singlePhaseVolt.includes(voltage)) {
        flc = singlePhaseFLC[hpStr][voltage];
    } else if (phase === 2 && threePhaseHp.includes(hp) && threePhaseVolt.includes(voltage)) {
        flc = threePhaseFLC[hpStr][voltage];
    }

    return flc;
};

export const lookupHP = (phase: 1 | 2, curr: number, voltage: number) => {
    let hp = 0;

    if (phase === 1) {
        for (const motor in singlePhaseFLC) {
            if (singlePhaseFLC[motor][voltage] === curr) {
                hp = singlePhaseFLC[motor][voltage];
            }
        }
    }

    if (phase === 2) {
        for (const motor in threePhaseFLC) {
            if (threePhaseFLC[motor][voltage] === curr) {
                hp = threePhaseFLC[motor]['Motor rating'];
            }
        }
    }

    return hp;
}

export const sizeOverloads = (flc: number, serviceFactor: number) => {
    const overloadSize = serviceFactor < 1.15 ? flc * 1.15 : flc * 1.25;
    return Math.round(overloadSize * 10) / 10;
}

export const sizeOvercurrent = (flc: number, protection: 'td' | 'ntd' | 'cb') => {
    let overcurrentSize = 0;
    switch (protection) {
        case 'td':
            overcurrentSize = flc * 1.75;
            break;
        case 'ntd':
            overcurrentSize = flc * 3;
            break;
        case 'cb':
            overcurrentSize = flc * 2.5;
        default:
            break;
    }
    return Math.round(overcurrentSize * 10) / 10;
}

export const lookupConduitFill = (conductor: string, conduit: string) => {
    let result = 0;

    for (const item of table6a) {
        if (item['Conductor size'] === conductor) {
            result = item[conduit];
        }
    }

    return result;
}

export const parseConduitSize = (size: number) => {
    let result = '';
    switch (size) {
        case 0.5:
            result = '1/2" (16)';
            break;
        case 0.75:
            result = '3/4" (21)';
            break;
        case 1:
            result = '1" (27)';
            break;
        case 1.25:
            result = '1 1/4" (35)';
            break;
        case 1.5:
            result = '1 1/2" (41)';
            break;
        case 2:
            result = '2" (53)';
            break;
        case 2.5:
            result = '2 1/2" (63)';
            break;
        case 3:
            result = '3" (78)';
            break;
        case 3.5:
            result = '3 1/2" (91)';
            break;
        case 4:
            result = '4" (103)';
            break;
        case 4.5:
            result = '4 1/2" (116)';
            break;
        case 5:
            result = '5" (129)';
            break;
        case 6:
            result = '6" (155)';
            break;
        case 8:
            result = '8" (200)';
            break;
        default:
            break;
    }

    return result;
}

export const lookupConduitSize = (conductor: string, numConductors: number) => {
    const result = [];

    for (const item of table6a) {
        if (item['Conductor size'] === conductor) {
            for (const key in item) {
                if (item[key] >= numConductors && key !== 'Conductor size') {
                    result.push(parseFloat(key));
                }
            }
        }
    }

    return parseConduitSize(Math.min(...result));
}